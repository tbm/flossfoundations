---
layout: single
title: "Links from 2008 Foundation Summit"
permalink: /node/16
---

* [OpenERP](http://www.openerp.com/)
* [Donorware](http://www.donorware.org/)
* [CiviCRM](http://www.civicrm.org/)
* [PentaBarf](http://www.pentabarf.org/)
* [Non Profit Tech Conference](http://www.ntenonline.org/)
* [SPI](http://www.spi-inc.org/)
* [Software Conservancy](http://conservancy.softwarefreedom.org/)
* [ID Commons](http://wiki.idcommons.net/index.php/Main_Page)
* [Higgins](http://www.eclipse.org/higgins/)
* [Foss Leader course](http://www.planetwork.net/fossleaders/)
* [FossCoach](http://fosscoach.wikia.com/wiki/FOSSCoach)
* [E-Myth Revisited](http://www.amazon.com/E-Myth-Revisited-Small-Businesses-About/dp/0887307280) book. [E-Myth Website](http://www.e-myth.com/pub/htdocs/about)
* [First Break All the Rules book](http://www.amazon.com/First-Break-All-Rules-Differently/dp/0684852861)
* [Pay Simple](http://www.paysimple.com/)
* [Tip Joy](http://tipjoy.com/)
* [Apache CLA](http://www.apache.org/licenses/icla.txt)
* [Zazzle](http://www.zazzle.com/)
* [Yelp](http://www.yelp.com/)
* [Floss Foundations Schwag page](http://flossfoundations.org/node/20)
* [OSEE](http://www.eclipse.org/proposals/osee/)
* [Autonomous](http://www.autonomo.us/)
* [Identica](http://identi.ca/)
* [Sun's JCA](http://www.sun.com/software/opensource/contributor_agreement.jsp) is CC licensed.
* [Avoir](http://avoir.uwc.ac.za/)
* FOSS-Universities Google group
* [University of Calais free software masters program](http://dpt-info.univ-littoral.fr/mediawiki/index.php/I2L:Accueil)
* [Trademark policies](http://flossfoundations.org/node/21)
* [OMS CODEC](http://blogs.sun.com/openmediacommons/entry/oms_video_a_project_of)
* Donations Pages:
    * [Apache](http://www.apache.org/foundation/contributing.html)
    * [Eclipse.org Donate](http://www.eclipse.org/donate)
    * [FSF](https://www.fsf.org/associate/support_freedom/)
    * [Mozilla](http://www.mozilla.org/foundation/donate.html)
* Fund raising resources:
    * [The Fund Raising School at Indiana University](http://www.philanthropy.iupui.edu/TheFundRaisingSchool/) - one of the best sources in the US for training on fund raising
