---
layout: single
title: "Schwag Resources"
permalink: /node/20
---

Links:

http://geekgirl.dk/linux/merchandise/

Might provide advice: Wim Vandeputte, wvdputte@kd85.com, http://kd85.com/

---

## North America

### Dormers Screenprint

```
112 S Edwards St
Newberg, OR 97132 (45 minutes from downtown Portland)
503-538-1234
```

Comments: Used with success by The Linux Fund for T-shirts and Buttons and Banners. Reasonable pricing, not killer, but excellent customer service including short deadlines

Feel free to add beautiful formatting.

---

Used by PDX PERL Mongers in 2008: Hyder Graphics in Beaverton.  

~$12/shirt for 52 with the american apparel shirts and a 
single color on each side.  Around 7 days turnaround.

Used by PDX PERL Mongers in 2006: bluepdx.com in SE.  They were 
pretty good to work with and have supposedly done a quicker turnaround 
when they have shirts on-hand. Perhaps no USA-made shirts.

1-800-BIG-DOGS or email CorporateSales@BigDogs.com

Comments: Attractive pricing and quality, not yet verified with an order.

---

http://www.cafepress.com/
Comments: quality and price concerns

---

## Europe

http://www.kernelconcepts.de/

Comments: Can produce pins, fobs and other non-shirt items

If you want a custom-printed memory stick in Europe, harass Michael Dexter dexter@bsdfund.org for the link he would need to dig up.
